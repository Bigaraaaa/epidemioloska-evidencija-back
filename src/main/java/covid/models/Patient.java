package covid.models;

import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Patient {

	@Id
	private String id;
	
	private String clinicBranch;
	
	private String citizenship;
	
	private String countryOfImport;
	
//	@DBRef
	private Set<Contact> contact;
	
//	@DBRef
	private Set<Mesure> mesure;
	
//	@DBRef
	private Set<Status> status;
	
//	@DBRef
	private PersonalInfo personalInfo;
	
	
	public Patient() {
		
	}


	public Patient(String clinicBranch, String citizenship, String countryOfImport, Set<Contact> contact,
			Set<Mesure> mesure, Set<Status> status, PersonalInfo personalInfo) {
		super();
		this.clinicBranch = clinicBranch;
		this.citizenship = citizenship;
		this.countryOfImport = countryOfImport;
		this.contact = contact;
		this.mesure = mesure;
		this.status = status;
		this.personalInfo = personalInfo;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getClinicBranch() {
		return clinicBranch;
	}


	public void setClinicBranch(String clinicBranch) {
		this.clinicBranch = clinicBranch;
	}


	public String getCitizenship() {
		return citizenship;
	}


	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}


	public String getCountryOfImport() {
		return countryOfImport;
	}


	public void setCountryOfImport(String countryOfImport) {
		this.countryOfImport = countryOfImport;
	}


	public Set<Contact> getContact() {
		return contact;
	}


	public void setContact(Set<Contact> contact) {
		this.contact = contact;
	}


	public Set<Mesure> getMesure() {
		return mesure;
	}


	public void setMesure(Set<Mesure> mesure) {
		this.mesure = mesure;
	}


	public Set<Status> getStatus() {
		return status;
	}


	public void setStatus(Set<Status> status) {
		this.status = status;
	}


	public PersonalInfo getPersonalInfo() {
		return personalInfo;
	}


	public void setPersonalInfo(PersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}

	
	
}
