package covid.models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Mesure {
	
	@Id
	private String id;

	private String mesure;
	
	private Date startDate;
	
	private Date endDate;
	
	private String institution;
	
	private String rescriptionNumber;
	
	public Mesure() {
		
	}
	
	public Mesure(String mesure, Date startDate, Date endDate, String institution, String rescriptionNumber) {
		super();
		this.mesure = mesure;
		this.startDate = startDate;
		this.endDate = endDate;
		this.institution = institution;
		this.rescriptionNumber = rescriptionNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMesure() {
		return mesure;
	}

	public void setMesure(String mesure) {
		this.mesure = mesure;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getRescriptionNumber() {
		return rescriptionNumber;
	}

	public void setRescriptionNumber(String rescriptionNumber) {
		this.rescriptionNumber = rescriptionNumber;
	}

	
}
