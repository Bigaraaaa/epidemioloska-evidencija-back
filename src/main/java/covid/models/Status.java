package covid.models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Status {

	@Id
	private String id;
	
	private Date date;
	
	private float temperature;
	
	private String description;
	
	private String status;
	
	private String anamneses;
	
	
	public Status() {
		
	}
	
	public Status(Date date, float temperature, String description, String status, String anamneses) {
		super();
		this.date = date;
		this.temperature = temperature;
		this.description = description;
		this.status = status;
		this.anamneses = anamneses;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAnamneses() {
		return anamneses;
	}

	public void setAnamneses(String anamneses) {
		this.anamneses = anamneses;
	}

	
	
}
