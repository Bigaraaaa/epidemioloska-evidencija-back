package covid.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import covid.models.Doctor;

@Repository
public interface DoctorRepository extends MongoRepository<Doctor, String> {

}
