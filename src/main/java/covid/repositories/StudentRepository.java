package covid.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import covid.models.Student;

@Repository
public interface StudentRepository extends MongoRepository<Student, String> {

}
