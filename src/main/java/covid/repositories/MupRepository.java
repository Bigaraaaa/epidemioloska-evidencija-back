package covid.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import covid.models.Mup;

@Repository
public interface MupRepository extends MongoRepository<Mup, String> {

}
