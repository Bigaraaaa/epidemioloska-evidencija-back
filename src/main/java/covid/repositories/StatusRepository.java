package covid.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import covid.models.Status;

@Repository
public interface StatusRepository extends MongoRepository<Status, Long> {

	Optional<Status> getById(String id);
}
