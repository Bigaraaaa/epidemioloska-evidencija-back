package covid.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import covid.models.Patient;

@Repository
public interface PatientRepository extends MongoRepository<Patient, Long> {

	Optional<Patient> getById(String id);
}
