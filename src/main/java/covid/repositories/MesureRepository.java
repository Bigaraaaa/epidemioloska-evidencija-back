package covid.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import covid.models.Mesure;

@Repository
public interface MesureRepository extends MongoRepository<Mesure, Long> {

	Optional<Mesure> getById(String id);
}
