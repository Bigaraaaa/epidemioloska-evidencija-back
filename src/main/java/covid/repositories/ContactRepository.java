package covid.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import covid.models.Contact;

@Repository
public interface ContactRepository extends MongoRepository<Contact, Long> {

	Optional<Contact> getById(String id);
}
