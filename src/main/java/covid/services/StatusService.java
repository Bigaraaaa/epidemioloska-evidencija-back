package covid.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import covid.models.PersonalInfo;
import covid.models.Status;
import covid.repositories.StatusRepository;

@Service
public class StatusService {

	@Autowired
	private StatusRepository statusRepository;
	
	public Iterable<Status> getAll(){
		return statusRepository.findAll();
	}
	
	public Optional<Status> getById(String id) {
		return statusRepository.getById(id);
	}
	
	public void addStatus(Status status) {
		statusRepository.save(status);
	}
	
	public void editStatus(String id, Status status) {
		Optional<Status> s = statusRepository.getById(id);
		
		if(s.isPresent()) {
			status.setId(s.get().getId());
			statusRepository.save(status);
		}
	}
	
	public void deleteStatus(String id) {
		Optional<Status> s = statusRepository.getById(id);
		
		if(s.isPresent()) {
			statusRepository.delete(s.get());
		}
	}
}
