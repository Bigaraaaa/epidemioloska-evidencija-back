package covid.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import covid.models.AccountData;
import covid.models.AuthProvider;
import covid.models.Mup;
import covid.models.Student;
import covid.repositories.AccountDataRepository;
import covid.repositories.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private AccountDataService accountDataService;
	
	@Autowired
	private AccountDataRepository accountDataRepository;
	
	@Autowired
	private PermissionService perrmissionService;
	
	public Iterable<Student> getAll() {
		return studentRepository.findAll();
	}
	
	public Optional<Student> getById(String id){
		return studentRepository.findById(id);
	}
	
	public HttpStatus addStudent(Student student) {
		
		Optional<AccountData> accountData = accountDataRepository.getByPhoneNumber(student.getAccountData().getPhoneNumber());
		
		if(accountData.isPresent()) {
			return HttpStatus.IM_USED;
		} else {
			accountDataService.addStudentAccountData(student.getAccountData());
			student.getAccountData().setProvider(AuthProvider.local);
			studentRepository.save(student);
			
			return HttpStatus.CREATED;
		}	
	}
	
	public void editStudent(String id, Student student) {
		
		Optional<Student> s = studentRepository.findById(id);
		
		if(s.isPresent()) {
			student.setId(s.get().getId());
			accountDataService.editAccountData(s.get().getAccountData().getId(), s.get().getAccountData());
			
			studentRepository.save(student);
		}
	}
	
	public void deleteStudent(String id) {
		Optional<Student> s = studentRepository.findById(id);
		
		if (s.isPresent()) {
			accountDataService.deleteUser(s.get().getAccountData().getId());
			studentRepository.delete(s.get());
		}
		
	}
}
