package covid.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import covid.models.AccountData;
import covid.models.AuthProvider;
import covid.models.Doctor;
import covid.repositories.AccountDataRepository;
import covid.repositories.DoctorRepository;

@Service
public class DoctorService {

	@Autowired
	private DoctorRepository doctorRepository;
	
	@Autowired
	private AccountDataService accountDataService;
	
	@Autowired
	private AccountDataRepository accountDataRepository;
	
	@Autowired
	private PermissionService perrmissionService;
	
	public Iterable<Doctor> getAll() {
		return doctorRepository.findAll();
	}
	
	public Optional<Doctor> getById(String id){
		return doctorRepository.findById(id);
	}
	
	public HttpStatus addDoctor(Doctor doctor) {
		
		Optional<AccountData> accountData = accountDataRepository.getByPhoneNumber(doctor.getAccountData().getPhoneNumber());
		
		if(accountData.isPresent()) {
			return HttpStatus.IM_USED;
		} else {
			accountDataService.addDoctorAccountData(doctor.getAccountData());
			doctor.getAccountData().setProvider(AuthProvider.local);
			doctorRepository.save(doctor);
			
			return HttpStatus.CREATED;
		}
		
	}
	
	public void editDoctor(String id, Doctor doctor) {
		
		Optional<Doctor> d = doctorRepository.findById(id);
		
		if(d.isPresent()) {
			doctor.setId(d.get().getId());
			accountDataService.editAccountData(d.get().getAccountData().getId(), d.get().getAccountData());
			
			doctorRepository.save(doctor);
		}
	}
	
//	public void editDoctorPin(String id, AccountData accountData) {
//		Optional<AccountData> u = accountDataRepository.findById(id);
//		
//		if(u.isPresent()) {
//			accountData.setId(u.get().getId());
//			accountData.setPhoneNumber(u.get().getPhoneNumber());
//			accountData.setSurname(u.get().getSurname());
//			accountData.setName(u.get().getName());
//			accountData.setPin(passwordEncoder.encode(accountData.getPin()));
//			accountDataRepository.save(accountData);
//		}
//	}
	
	public void deleteDoctor(String id) {
		Optional<Doctor> d = doctorRepository.findById(id);
		
		if (d.isPresent()) {
			accountDataService.deleteUser(d.get().getAccountData().getId());
			doctorRepository.delete(d.get());
		}
		
	}

	
}
