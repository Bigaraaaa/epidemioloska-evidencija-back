package covid.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import covid.models.Address;
import covid.models.Contact;
import covid.repositories.ContactRepository;

@Service
public class ContactService {

	@Autowired
	private ContactRepository contactRepository;
	
	public Iterable<Contact> getAll() {
		return contactRepository.findAll();
	}
	
	public Optional<Contact> getById(String id) {
		return contactRepository.getById(id);
	}
	
	public void addContact(Contact contact) {
		contactRepository.save(contact);
	}
	
	public void editContact(String id, Contact contact) {
		Optional<Contact> c = contactRepository.getById(id);
		
		if(c.isPresent()) {
			contact.setId(c.get().getId());
			contactRepository.save(contact);
		}
	}
	
	public void deleteContact(String id) {
		Optional<Contact> c = contactRepository.getById(id);
		
		if(c.isPresent()) {
			contactRepository.delete(c.get());
		}
	}
	
}
