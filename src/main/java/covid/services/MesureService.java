package covid.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import covid.models.Contact;
import covid.models.Mesure;
import covid.repositories.MesureRepository;

@Service
public class MesureService {

	@Autowired
	private MesureRepository mesureRepository;
	
	public Iterable<Mesure> getAll() {
		return mesureRepository.findAll();
	}
	
	public Optional<Mesure> getById(String id) {
		return mesureRepository.getById(id);
	}
	
	public void addMesure(Mesure mesure) {
		mesureRepository.save(mesure);
	}
	
	public void editMesure(String id, Mesure mesure) {
		Optional<Mesure> m = mesureRepository.getById(id);
		
		if(m.isPresent()) {
			mesure.setId(m.get().getId());
			mesureRepository.save(mesure);
		}
	}
	
	public void deleteMesure(String id) {
		Optional<Mesure> m = mesureRepository.getById(id);
		
		if(m.isPresent()) {
			mesureRepository.delete(m.get());
		}
	}
}
