package covid.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import covid.models.Permission;
import covid.repositories.PermissionRepository;

@Service
public class PermissionService {

	@Autowired
	private PermissionRepository permissionRepository;
	
	public void addDoctorPermission(Permission permission) {
		permission.setAuthority("ROLE_DOCTOR");
		permissionRepository.save(permission);
	}
	
	public void addAdministratorPermission(Permission permission) {
		permission.setAuthority("ROLE_ADMINISTRATOR");
		permissionRepository.save(permission);
	}
	
	public void addStudentPermission(Permission permission) {
		permission.setAuthority("ROLE_STUDENT");
		permissionRepository.save(permission);
	}
	
	public void addMupPermission(Permission permission) {
		permission.setAuthority("ROLE_MUP");
		permissionRepository.save(permission);
	}
}
