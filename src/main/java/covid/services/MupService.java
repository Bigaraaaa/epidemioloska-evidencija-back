package covid.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import covid.models.AccountData;
import covid.models.AuthProvider;
import covid.models.Mup;
import covid.repositories.AccountDataRepository;
import covid.repositories.MupRepository;

@Service
public class MupService {
	
	@Autowired
	private MupRepository mupRepository;

	@Autowired
	private AccountDataService accountDataService;
	
	@Autowired
	private AccountDataRepository accountDataRepository;
	
	public Iterable<Mup> getAll() {
		return mupRepository.findAll();
	}
	
	public Optional<Mup> getById(String id){
		return mupRepository.findById(id);
	}
	
	public HttpStatus addMup(Mup mup) {
		
		Optional<AccountData> accountData = accountDataRepository.getByPhoneNumber(mup.getAccountData().getPhoneNumber());
		
		if(accountData.isPresent()) {
			return HttpStatus.IM_USED;
		} else {
			accountDataService.addMupAccountData(mup.getAccountData());
			mup.getAccountData().setProvider(AuthProvider.local);
			mupRepository.save(mup);
			
			return HttpStatus.CREATED;
		}
		
	}
	
	public void editMup(String id, Mup mup) {
		
		Optional<Mup> m = mupRepository.findById(id);
		
		if(m.isPresent()) {
			mup.setId(m.get().getId());
			accountDataService.editAccountData(m.get().getAccountData().getId(), m.get().getAccountData());
			
			mupRepository.save(mup);
		}
	}
	
	public void deleteMup(String id) {
		Optional<Mup> m = mupRepository.findById(id);
		
		if (m.isPresent()) {
			accountDataService.deleteUser(m.get().getAccountData().getId());
			mupRepository.delete(m.get());
		}
		
	}
}
