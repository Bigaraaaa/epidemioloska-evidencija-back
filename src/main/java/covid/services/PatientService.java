package covid.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import covid.models.Contact;
import covid.models.Patient;
import covid.models.Status;
import covid.repositories.PatientRepository;

@Service
public class PatientService {

	@Autowired
	private PatientRepository patientRepository;
	
	public Iterable<Patient> getAll() {
		return patientRepository.findAll();
	}
	
	public Optional<Patient> getById(String id) {
		return patientRepository.getById(id);
	}
	
	public void addPatient(Patient patient) {
		patientRepository.save(patient);
	}
	
	public void editPatient(String id, Patient patient) {
		Optional<Patient> p = patientRepository.getById(id);
		
		if(p.isPresent()) {
			patient.setId(p.get().getId());
			patientRepository.save(patient);
		}
	}
	
	public void deletePatient(String id) {
		Optional<Patient> p = patientRepository.getById(id);
		
		if(p.isPresent()) {
			patientRepository.delete(p.get());
		}
	}
	
	public void addPatientContact(String id, Contact contact) {
        Optional<Patient> patient = patientRepository.getById(id);
        if(patient.isPresent()) {
            Patient p = patient.get();
            p.getContact().add(contact);
            patientRepository.save(p);
        }
    }

	public void addPatientStatus(String id, Status status) {
        Optional<Patient> patient = patientRepository.getById(id);
        if(patient.isPresent()) {
            Patient p = patient.get();
            p.getStatus().add(status);
            patientRepository.save(p);
        }
    }
}
