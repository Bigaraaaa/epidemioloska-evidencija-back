package covid.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import covid.models.Mesure;
import covid.services.MesureService;

@RestController
@RequestMapping("/mesure")
public class MesureController {

	@Autowired
	private MesureService mesureService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
    public ResponseEntity<Iterable<Mesure>> getMesures() {
        return new ResponseEntity<Iterable<Mesure>>(mesureService.getAll(), HttpStatus.OK);
    }
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Mesure> geMesureById(@PathVariable String id) {
        Optional<Mesure> mesure = mesureService.getById(id);
        if(mesure.isPresent()) {
            return new ResponseEntity<Mesure>(mesure.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Mesure>(HttpStatus.NOT_FOUND);
    }
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
    public ResponseEntity<Mesure> addMesuret(@RequestBody Mesure mesure) {
		mesureService.addMesure(mesure);
        return new ResponseEntity<Mesure>(mesure, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Mesure> editMesure(@PathVariable String id, @RequestBody Mesure mesure){
		mesureService.editMesure(id, mesure);
		return new ResponseEntity<Mesure>(mesure, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Mesure> removeMesure(@PathVariable String id) {
        try {
        	mesureService.deleteMesure(id);
        }catch (Exception e) {
            return new ResponseEntity<Mesure>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Mesure>(HttpStatus.NO_CONTENT);
    }

}
