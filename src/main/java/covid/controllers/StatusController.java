package covid.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import covid.models.Status;
import covid.services.StatusService;

@RestController
@RequestMapping("/status")
public class StatusController {

	@Autowired
	private StatusService statusService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public ResponseEntity<Iterable<Status>> getStatuss() {
	    return new ResponseEntity<Iterable<Status>>(statusService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Status> geStatusById(@PathVariable String id) {
        Optional<Status> status = statusService.getById(id);
        if(status.isPresent()) {
            return new ResponseEntity<Status>(status.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Status>(HttpStatus.NOT_FOUND);
    }
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
    public ResponseEntity<Status> addStatus(@RequestBody Status status) {
		statusService.addStatus(status);
        return new ResponseEntity<Status>(status, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Status> editStatus(@PathVariable String id, @RequestBody Status status){
		statusService.editStatus(id, status);
		return new ResponseEntity<Status>(status, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Status> removeStatus(@PathVariable String id) {
        try {
        	statusService.deleteStatus(id);
        }catch (Exception e) {
            return new ResponseEntity<Status>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Status>(HttpStatus.NO_CONTENT);
    }
}
