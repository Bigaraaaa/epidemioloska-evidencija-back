package covid.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import covid.models.PersonalInfo;
import covid.services.PersonalInfoService;

@RestController
@RequestMapping("/personal-info")
public class PersonalInfoController {

	@Autowired
	private PersonalInfoService personalInfoService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
    public ResponseEntity<Iterable<PersonalInfo>> getPersonalInfo() {
        return new ResponseEntity<Iterable<PersonalInfo>>(personalInfoService.getAll(), HttpStatus.OK);
    }
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<PersonalInfo> gePersonalInfoById(@PathVariable String id) {
        Optional<PersonalInfo> personalInfo = personalInfoService.getById(id);
        if(personalInfo.isPresent()) {
            return new ResponseEntity<PersonalInfo>(personalInfo.get(), HttpStatus.OK);
        }
        return new ResponseEntity<PersonalInfo>(HttpStatus.NOT_FOUND);
    }
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
    public ResponseEntity<PersonalInfo> addPersonalInfo(@RequestBody PersonalInfo personalInfo) {
		personalInfoService.addPersonalInfo(personalInfo);
        return new ResponseEntity<PersonalInfo>(personalInfo, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<PersonalInfo> editPersonalInfo(@PathVariable String id, @RequestBody PersonalInfo personalInfo){
		personalInfoService.editPersonalInfo(id, personalInfo);
		return new ResponseEntity<PersonalInfo>(personalInfo, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<PersonalInfo> removePersonalInfo(@PathVariable String id) {
        try {
        	personalInfoService.deletePersonalInfo(id);
        }catch (Exception e) {
            return new ResponseEntity<PersonalInfo>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<PersonalInfo>(HttpStatus.NO_CONTENT);
    }
}
