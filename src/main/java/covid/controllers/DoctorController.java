package covid.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import covid.models.Doctor;
import covid.services.DoctorService;

@RestController
@RequestMapping("/doctor")
public class DoctorController {
	
	@Autowired
	private DoctorService doctorService;

    @RequestMapping(value="/all", method=RequestMethod.GET)
    public ResponseEntity<Iterable<Doctor>> getAll() {
        return new ResponseEntity<Iterable<Doctor>>(doctorService.getAll(), HttpStatus.OK);
    }
	

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Doctor> getById(@PathVariable String id) {
        Optional<Doctor> doctor = doctorService.getById(id);
        if(doctor.isPresent()) {
            return new ResponseEntity<Doctor>(doctor.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Doctor>(HttpStatus.NOT_FOUND);
    }
	
    @RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<Doctor> addAdministrator(@RequestBody Doctor doctor){
    	doctorService.addDoctor(doctor);
		return new ResponseEntity<Doctor>(doctor, HttpStatus.CREATED);
	}
	
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Doctor> editDoctor(@PathVariable String id, @RequestBody Doctor doctor){
		doctorService.editDoctor(id, doctor);
		return new ResponseEntity<Doctor>(doctor, HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/password/{id}", method = RequestMethod.PUT)
//	public ResponseEntity<Doctor> editDoctorPin(@PathVariable String id, @RequestBody Doctor user){
//		doctorService.ed
//		return new ResponseEntity<Doctor>(user, HttpStatus.OK);
//	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Doctor> removeDoctor(@PathVariable String id) {
        try {
        	doctorService.deleteDoctor(id);
        }catch (Exception e) {
            return new ResponseEntity<Doctor>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Doctor>(HttpStatus.NO_CONTENT);
    }

}
