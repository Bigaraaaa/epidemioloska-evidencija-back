package covid.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import covid.models.Administrator;
import covid.models.Mup;
import covid.services.MupService;

@RestController
@RequestMapping("/mup")
public class MupController {

	@Autowired
	private MupService mupService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
    public ResponseEntity<Iterable<Mup>> getAll() {
        return new ResponseEntity<Iterable<Mup>>(mupService.getAll(), HttpStatus.OK);
    }
	

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Mup> getById(@PathVariable String id) {
        Optional<Mup> mup = mupService.getById(id);
        if(mup.isPresent()) {
            return new ResponseEntity<Mup>(mup.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Mup>(HttpStatus.NOT_FOUND);
    }
	
    @RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<Mup> addMup(@RequestBody Mup mup){
    	mupService.addMup(mup);
		return new ResponseEntity<Mup>(mup, HttpStatus.CREATED);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
   	public ResponseEntity<Mup> editMup(@PathVariable String id, @RequestBody Mup mup){
   		mupService.editMup(id, mup);
   		return new ResponseEntity<Mup>(mup, HttpStatus.OK);
   	}
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Mup> removeMup(@PathVariable String id) {
        try {
        	mupService.deleteMup(id);
        }catch (Exception e) {
            return new ResponseEntity<Mup>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Mup>(HttpStatus.NO_CONTENT);
    }
}
