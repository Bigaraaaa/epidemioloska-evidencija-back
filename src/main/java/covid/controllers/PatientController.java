package covid.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import covid.models.Contact;
import covid.models.Patient;
import covid.models.Status;
import covid.services.PatientService;

@RestController
@RequestMapping("/patient")
public class PatientController {

	@Autowired
	private PatientService patientService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
    public ResponseEntity<Iterable<Patient>> getPatientss() {
        return new ResponseEntity<Iterable<Patient>>(patientService.getAll(), HttpStatus.OK);
    }
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Patient> gePatientById(@PathVariable String id) {
        Optional<Patient> patient = patientService.getById(id);
        if(patient.isPresent()) {
            return new ResponseEntity<Patient>(patient.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
    }
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
    public ResponseEntity<Patient> addPatient(@RequestBody Patient patient) {
        patientService.addPatient(patient);
        return new ResponseEntity<Patient>(patient, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Patient> editPatient(@PathVariable String id, @RequestBody Patient patient){
		patientService.editPatient(id, patient);
		return new ResponseEntity<Patient>(patient, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/add/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Contact> editPatientContact(@PathVariable String id, @RequestBody Contact contact){
		System.out.println(id);
		System.out.println(contact);
		patientService.addPatientContact(id, contact);
		return new ResponseEntity<Contact>(contact, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addstatus/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Status> editPatientStatus(@PathVariable String id, @RequestBody Status status){
		System.out.println(id);
		System.out.println(status);
		patientService.addPatientStatus(id, status);
		return new ResponseEntity<Status>(status, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Patient> removePatient(@PathVariable String id) {
        try {
        	patientService.deletePatient(id);
        }catch (Exception e) {
            return new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Patient>(HttpStatus.NO_CONTENT);
    }
}
