package covid.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import covid.models.Student;
import covid.services.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
    public ResponseEntity<Iterable<Student>> getAll() {
        return new ResponseEntity<Iterable<Student>>(studentService.getAll(), HttpStatus.OK);
    }
	

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Student> getById(@PathVariable String id) {
        Optional<Student> student = studentService.getById(id);
        if(student.isPresent()) {
            return new ResponseEntity<Student>(student.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);
    }
	
    @RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<Student> addStudent(@RequestBody Student student){
    	studentService.addStudent(student);
		return new ResponseEntity<Student>(student, HttpStatus.CREATED);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
   	public ResponseEntity<Student> editStudent(@PathVariable String id, @RequestBody Student student){
   		studentService.editStudent(id, student);
   		return new ResponseEntity<Student>(student, HttpStatus.OK);
   	}
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Student> removeStudent(@PathVariable String id) {
        try {
        	studentService.deleteStudent(id);
        }catch (Exception e) {
            return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Student>(HttpStatus.NO_CONTENT);
    }
}
