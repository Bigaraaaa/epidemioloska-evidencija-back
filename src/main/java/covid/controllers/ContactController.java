package covid.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import covid.models.Contact;
import covid.services.ContactService;

@RestController
@RequestMapping("/contact")
public class ContactController {

	@Autowired
	private ContactService contactService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
    public ResponseEntity<Iterable<Contact>> getContacts() {
        return new ResponseEntity<Iterable<Contact>>(contactService.getAll(), HttpStatus.OK);
    }
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Contact> getUserById(@PathVariable String id) {
        Optional<Contact> contact = contactService.getById(id);
        if(contact.isPresent()) {
            return new ResponseEntity<Contact>(contact.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Contact>(HttpStatus.NOT_FOUND);
    }
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
    public ResponseEntity<Contact> addContact(@RequestBody Contact contact) {
		contactService.addContact(contact);
        return new ResponseEntity<Contact>(contact, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Contact> editContact(@PathVariable String id, @RequestBody Contact contact){
		contactService.editContact(id, contact);
		return new ResponseEntity<Contact>(contact, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Contact> removeContact(@PathVariable String id) {
        try {
        	contactService.deleteContact(id);
        }catch (Exception e) {
            return new ResponseEntity<Contact>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Contact>(HttpStatus.NO_CONTENT);
    }

}
