package covid.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import covid.models.AccountData;
import covid.models.AuthProvider;

public class UserPrincipal implements OAuth2User, UserDetails {
   
	private String id;
    private String phoneNumber;
    private String pin;
    private boolean emailVerified;
    private Collection<? extends GrantedAuthority> authorities;
    private Map<String, Object> attributes;

    
    public UserPrincipal(String id, String phoneNumber, String pin, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.pin = pin;
        this.authorities = authorities;
    }

    public static UserPrincipal create(AccountData accountData) {
    	 if(accountData.getProvider() == AuthProvider.local) {
    		ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    			authorities.add(new SimpleGrantedAuthority(accountData.getPermission().getAuthority()));
    		return new UserPrincipal( accountData.getId(), accountData.getPhoneNumber(), accountData.getPin(), authorities);
    	 }
    		
    	return null;
        
    }

    public static UserPrincipal create(AccountData accountData, Map<String, Object> attributes) {
        UserPrincipal userPrincipal = UserPrincipal.create(accountData);
        userPrincipal.setAttributes(attributes);
        return userPrincipal;
    }


    public String getId() {
        return id;
    }

    

    public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
    public String getPassword() {
        return pin;
    }

    @Override
    public String getUsername() {
        return phoneNumber;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String getName() {
        return String.valueOf(id);
    }
}
